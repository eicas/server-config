# EICAS server config

## Initial deploy

On Hetzner:

(TODO update: 'git clone', git-crypt unlock, then infect)

Create a machine and add cloud-config:

```
#cloud-config

runcmd:
  - curl https://raw.githubusercontent.com/elitak/nixos-infect/master/nixos-infect | PROVIDER=hetznercloud NIX_CHANNEL=nixos-unstable NIXOS_CONFIG=https://codeberg.org/eicas/server-config/raw/branch/main/configuration.nix bash 2>&1 | tee /tmp/infect.log
```

Edit DNS so 'hetzner.eicas.nl' points to this IP, update SPF record to allow email through this IP

## Test if configuration builds

Copy the networking.nix and hardware-configuration.nix from the deployed machine:

    scp it@hetzner.eicas.nl:/etc/nixos/networking.nix .
    scp it@hetzner.eicas.nl:/etc/nixos/hardware-configuration.nix .

Then build:

    nixos-rebuild --flake .#hetzner build

## Update

Copy the networking.nix and hardware-configuration.nix from the deployed machine:

    scp it@hetzner.eicas.nl:/etc/nixos/networking.nix .
    scp it@hetzner.eicas.nl:/etc/nixos/hardware-configuration.nix .

Then deploy:

    nixos-rebuild --flake .#hetzner --use-remote-sudo --target-host it@hetzner.eicas.nl switch

## Secrets

Edit a secret:

    agenix -e ./mailinglists.yaml.age

Add a new secret:

* add to `secrets.nix`
* add `age.secrets.foo` to `configuration.nix`
* use as `config.age.secrets.foo.path`
## Administration

### Roundcube

Manual database connection: `nix-shell -p postgresql --run "sudo -u postgres psql roundcube"`

## TODO

* how to deploy and update mailrelay `/etc/mailrelay/*` (some of which are secret)
