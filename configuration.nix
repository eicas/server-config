{ config, pkgs, pisignage-server, admin-sync-src, ... }:

let
  # TODO config as hashmap, convert to PHP?
  myRoundcubePlugin = { pname, version, src, config }:
    let
      configfile = pkgs.writeTextFile {
        name = "${pname}-config-inc.php";
        text = config;
      };
    in
      pkgs.runCommand "roundcube-plugin-${pname}-${version}" { } ''
        mkdir -p $out/plugins/${pname}
        cp -r ${src}/* $out/plugins/${pname}
        cp ${configfile} $out/plugins/${pname}/config.inc.php
      '';
in
{
  imports = [
    ./hardware-configuration.nix
    ./networking.nix
    ./calendar-export.nix
    ./admin-sync.nix
    ./monitoring.nix
    ./outgoing-mailserver.nix
    ./mailrelay.nix
    ./nextcloud.nix
    #./photoprism.nix
    (pisignage-server.nixosModules.pisignage)
  ];

  documentation.nixos.enable = false;

  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (pkgs.lib.getName pkg) [
    # onlyoffice
    "corefonts"
  ];
  nixpkgs.config.permittedInsecurePackages = [
    # onlyoffice
    "v8-9.7.106.18"
  ];

  # temporary to allow gc
  nix.package = pkgs.nixVersions.latest;
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  age.secrets.acme.file = ./acme.age;
  age.secrets.nc.file = ./nc.age;
  age.secrets.nc-roundcube = {
    file = ./nc.age;
    owner = "roundcube";
    group = "roundcube";
  };
  age.secrets.ncadminpass = {
    file = ./ncadminpass.age;
    owner = "nextcloud";
    group = "nextcloud";
  };
  #age.secrets.onlyoffice-jwt = {
  #  file = ./onlyoffice-jwt.age;
  #  owner = "onlyoffice";
  #  group = "onlyoffice";
  #};
  age.secrets.mailinglistsYaml = {
    file = ./mailinglists.yaml.age;
    owner = "roundcube";
    group = "roundcube";
  };
  age.secrets.personalYaml = {
    file = ./personal.yaml.age;
    owner = "roundcube";
    group = "roundcube";
  };
  age.secrets.stats = {
    file = ./stats.age;
    owner = "stats";
    group = "stats";
  };

  boot.tmp.cleanOnBoot = true;

  nix.settings.trusted-users = [ "it" ];

  networking.hostName = "server";
  networking.domain = "eicas.nl";

  networking.firewall.allowedTCPPorts = [
    # redirect http to https
    80
    # webmail (and other sites?)
    443
    # SMTP (TLS and StartTLS)
    465 587
    # IMAP (TLS and StartTLS)
    993 143
  ];

  time.timeZone = "Europe/Amsterdam";

  services.mailrelay.enable = true;
  services.roundcube = {
    enable = true;
    hostName = "webmail.eicas.nl";
    extraConfig = ''
      $config['smtp_server'] = "tls://mail.eicas.nl";
      $config['default_host'] = "tls://mail.eicas.nl";
      # use login credentials
      $config['smtp_user'] = "%u";
      $config['smtp_pass'] = "%p";
      $config['plugins'] = array('globaladdressbook');
    '';
    package = pkgs.roundcube.withPlugins (plugins: [
      (myRoundcubePlugin {
        pname = "globaladdressbook";
        version = "2.1";
        #src = /home/aengelen/dev/roundcube-globaladdressbook;
        src = pkgs.fetchFromGitHub {
          owner = "johndoh";
          repo = "roundcube-globaladdressbook";
          rev = "tags/2.1";
          hash = "sha256-MmttArRdkLlLujFDC2zoALPujgmxgY941SnxelkWX/0=";
        };
        config = ''
          <?php
          $config['globaladdressbooks']['global'] = [
            'name' => 'EICAS adresboek',
            'user' => 'globaladdressbook@eicas.nl',
            'perms' => 0,
            'force_copy' => true,
            'groups' => true,
            'admin' => 'it@eicas.nl',
            'autocomplete' => true,
            'check_safe' => true,
            'visibility' => null,
          ];
          ?>
        '';
      })
    ]);
  };
  # used by roundcube, to upgrade we'd need to migrate the data
  services.postgresql.package = pkgs.postgresql_14;
  services.nginx.enable = true;
  security.acme = {
    acceptTerms = true;
    defaults = {
      email = "it@eicas.nl";
      dnsProvider = "cloudflare";
      environmentFile = "${config.age.secrets.acme.path}";
    };
  };

  services.pisignage = {
    enable = true;
  };
  services.nginx.virtualHosts."video.eicas.nl" = {
    forceSSL = true;
    enableACME = true;
    # force DNS-01 validation
    acmeRoot = null;
    http2 = true;
    locations."/" = {
      proxyPass = "http://127.0.0.1:3000";
      proxyWebsockets = true;
      extraConfig = ''
        client_max_body_size 3G;
      '';
    };
  };
  services.nginx.virtualHosts."www-test3.eicas.nl" = {
    forceSSL = true;
    enableACME = true;
    # force DNS-01 validation
    acmeRoot = null;
    http2 = true;
    root = ./www-test;
  };

  security.sudo.extraRules = [ {
    users = [ "it" ];
    commands = [ {
      command = "ALL";
      options = [ "NOPASSWD" ];
    } ];
  } ];

  services.openssh.enable = true;
  #https://github.com/NixOS/nixpkgs/pull/323753
  services.openssh.settings.LoginGraceTime = 0;

  users.mutableUsers = false;
  users.users.it = {
    isNormalUser = true;
    description = "EICAS IT";
    extraGroups = [ "wheel" ];
    hashedPassword = "$y$j9T$3n/m7.TkRZPgUbNcfSxmw1$FZoJDXX/dPf//UAjk4tj7bCYviXHWd8OA2hhEfkvLVA";
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+Q5zbwY6lm/HZgE7IGsT9Zo83v9Z+q+i6z+t31E2OHJgXuJHYE8rWloRpHGaVrffaQJyBp3hadvvMhqBdLq3QesU/51bhLcYr+T1esx30RHjp48NhcC0c9s39pHRIZ0oPU2FUP97YujSSBsIrbbcNYtNFjkLn6IfrFCzOVSl9BO1uhrnAuaFUMZk5ir56FzAHYUp6S3CGb4+zmH3upF0skmqnrJI0gkmvLpufoyNS/QtnY1ctRvKkqgY8xHhWBLEUSIYhY1SP9nl4TiRy7OUw692IiQjWoOdRLKwARMbkWjV9jEMZmmM7BAQDS+FfbAhJbDcXdQH7hum9uZEnipWR ssh-key"
    ];
  };
  system.stateVersion = "23.11";
}
