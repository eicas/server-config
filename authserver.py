import sys
import tomllib
from http import server

with open("/etc/mailrelay/creds.txt","rb") as file:
    creds = tomllib.load(file)

#authlog = open("auth-log.txt","a")
authlog = sys.stdout
authlog.write("Starting authserver\n")
authlog.flush()

class HTTPRequestHandler(server.SimpleHTTPRequestHandler):
    def end_headers(self):
        user = self.headers['Auth-User'].lower().replace("@eicas.nl", "")
        password = self.headers['Auth-Pass']
        password_redacted = password[0] + (len(password)-1) * '*'
        protocol = self.headers['Auth-Protocol']
        authlog.write("saw '%s' with pwd '%s' for %s\n" % (user, password_redacted, protocol))
        if not user in creds:
            authlog.write("Unknown user '%s' with pwd '%s'\n" % (user, password_redacted))
            self.send_header("Auth-Status", "Authentication failed")
        elif not (password.replace("%25", "%")) in creds[user]:
            authlog.write("User '%s' used unexpected pwd '%s'\n" % (user, password_redacted))
            self.send_header("Auth-Status", "Authentication failed")
        else:
            self.send_header("Auth-Server","127.0.0.1")
            if protocol == "imap":
                self.send_header("Auth-Pass", creds[user][0])
                self.send_header("Auth-Port", "1143")
                self.send_header("Auth-Status", "OK")
            elif protocol == "smtp":
                self.send_header("Auth-Status", "OK")
                #if user == "info" or user == "organisatie" or user == "suppoosten":
                if user == "it" or user == "bestuur" or user == "organisatie":
                    self.send_header("Auth-Port", "25")
                    self.send_header("Auth-User", user)
                    self.send_header("Auth-Method", "none")
                else:
                    self.send_header("Auth-Port", "2525")
                    self.send_header("Auth-Pass", creds[user][0])
            else:
                authlog.write("Unknown protocol: [%s]" % protocol)
                self.send_header("Auth-Status", "Authentication failed" % protocol)
        authlog.flush()

        server.SimpleHTTPRequestHandler.end_headers(self)

if __name__ == '__main__':
    server.test(HandlerClass=HTTPRequestHandler)
