let
  raboof = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMpM+jZrz5f2imQD71+pPmvDM5aGSyz/5VJH61g4Q9SF";
  marten = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDhKuiiJisvlskJOvvWejcV5DpLJP1gPptSg2BRaHjPQQTTaOFDQzfocD0xeKPGXPpyYYOP1c27v4dbU2A60zs+vI8pYOF4QlERuj5kzXGGeIJooBMaEVmT2eU+LTve4vA7hbV0+XbVTDQtnHS8ZEQ257iwTgUOQh2nmhzZuOqtwm8XANnqiZ/XG22e6+QGxmu5CAAj7Tcg+uufngSBBXf+MajoUHe5V68YldV5LJwOA1DHD0jQ8GFro3UxP8vvVpjdBjQtJ+nGtTJQ4lvKbDtx4vf1WnucYutOtodH4mkisfpS+ALTOhLRGaUQ/b/i0BTEjnLfG/Y/S2n39u6F4isF";
  hetzner = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIE09Le2VDO3DtdfjSCbqvRDYKSrMJQp1hGcPeljwKWUU";
  allPublicKeys = [ raboof marten hetzner ];
in
{
  "nc.age".publicKeys = allPublicKeys;
  "ncadminpass.age".publicKeys = allPublicKeys;
  "onlyoffice-jwt.age".publicKeys = allPublicKeys;
  "mailinglists.yaml.age".publicKeys = allPublicKeys;
  "personal.yaml.age".publicKeys = allPublicKeys;
  "stats.age".publicKeys = allPublicKeys;
  "acme.age".publicKeys = allPublicKeys;
  "notificaties.age".publicKeys = allPublicKeys;
}
