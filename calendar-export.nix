{ config, lib, pkgs, admin-sync-src, ... }:

let
  calendar-export = pkgs.callPackage admin-sync-src {};
in
{
  systemd.services.calendar-export = {
    script = ''
      echo $PWD
      cp ${./secrets/dotenv} ./.env
      ${calendar-export}/bin/kalender-algemeen
      ${calendar-export}/bin/suppoosten-rapportage

      source ${config.age.secrets.nc.path}
      ${pkgs.curl}/bin/curl -H "Authorization: Bearer $NC_TOKEN" --upload-file kalender.pdf https://cloud.eicas.nl/remote.php/dav/files/$NC_USER/Algemeen/kalender.pdf
      ${pkgs.curl}/bin/curl -H "Authorization: Bearer $NC_TOKEN" --upload-file suppoosten-rapportage.pdf https://cloud.eicas.nl/remote.php/dav/files/$NC_USER/Stafoverleg/suppoosten-rapportage.pdf
    '';
    serviceConfig.Type = "oneshot";
    serviceConfig.TimeoutStartSec = 2400;
  };
  systemd.timers.calendar-export = {
    partOf = [ "calendar-export.service" ];
    wantedBy = [ "timers.target" ];
    timerConfig.OnUnitActiveSec = 3600;
    timerConfig.OnBootSec = 900;
    timerConfig.RandomizedDelaySec = 900;
  };
}
