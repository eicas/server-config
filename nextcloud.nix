{ config, lib, pkgs, ... }:

let
  eicas-theme = pkgs.stdenv.mkDerivation {
    name = "EICAS theme";
    src = ./.;
    installPhase = ''
      runHook preInstall
      mkdir -p $out
      cat >$out/defaults.php <<EOF;
      <?php
      class OC_Theme {
        public function getTitle() {
          return 'EICAS Cloudd';
        }
      }
EOF
      runHook postInstall
    '';
  };
in
{
  environment.etc."nextcloud-admin-pass".text = "PWD";
  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud28;
    hostName = "cloud2.eicas.nl";
    config = {
      adminpassFile = config.age.secrets.ncadminpass.path;
      dbtype = "pgsql";
    };
    settings = {
      default_language = "nl";
      force_language = "nl";
      default_timezone = "Europe/Amsterdam";
      knowledgebaseenabled = false;
      logo_url = "https://cloud.eicas.nl";
      # 0=debug
      #loglevel = 0;
    };
    database.createLocally = true;
    https = true;
    extraAppsEnable = true;
    extraApps = with config.services.nextcloud.package.packages.apps; {
      external = pkgs.fetchNextcloudApp {
        url = "https://github.com/nextcloud-releases/external/releases/download/v5.3.1/external-v5.3.1.tar.gz";
        license = "agpl3Only";
        hash = "sha256-WJBu2KFLsT/p+iiwy0p5UdKdrSMrfD3dSQjwuAw6DwY=";
      };
      theming_customcss = pkgs.fetchNextcloudApp {
        url = "https://github.com/nextcloud/theming_customcss/archive/refs/tags/v1.17.0.tar.gz";
        license = "agpl3Only";
        hash = "sha256-aM55isdS+heh1nwJDJpS+oA8/ocPR/wrs07BGuARYHw=";
      };
      #documentserver_community = pkgs.fetchNextcloudApp {
      #  url = "https://github.com/ONLYOFFICE/onlyoffice-nextcloud/releases/download/v9.3.0/onlyoffice.tar.gz";
      #  license = "agpl3Only";
      #  hash = "sha256-rHUM3HVY3INPAtTqYxpm9V3Ad8VTl+Wd2S7xAj0CJko=";
      #};
      inherit calendar deck groupfolders;
      # onlyoffice;
    };
  };
  services.nginx.virtualHosts.${config.services.nextcloud.hostName} = {
    forceSSL = true;
    enableACME = true;
    # force DNS-01 validation
    acmeRoot = null;
  };
  #services.onlyoffice = {
  #  enable = true;
  #  hostname = "onlyoffice.eicas.nl";
  #  port = 8001;
  #  jwtSecretFile = config.age.secrets.onlyoffice-jwt.path;
  #};
  #services.nginx.virtualHosts.${config.services.onlyoffice.hostname} = {
  #  forceSSL = true;
  #  enableACME = true;
  #  # force DNS-01 validation
  #  acmeRoot = null;
  #};
  systemd.services.nextcloud-custom-config = {
    path = [
      config.services.nextcloud.occ
    ];
    script = let
      ext = {
        "1" = {
          id = "1";
          name = "Statistieken";
          url = "https://stats.eicas.nl";
          type = "link";
          icon = "clip.svg";
        };
      };
    in ''
      nextcloud-occ theming:config name "EICAS Cloud"
      nextcloud-occ theming:config url https://cloud.eicas.nl
      nextcloud-occ theming:config privacyUrl https://www.eicas.nl/privacy
      nextcloud-occ theming:config color "#3253a5"
      nextcloud-occ theming:config logo ${./nextcloud/logo.png}
      nextcloud-occ theming:config background backgroundColor

      nextcloud-occ app:disable dashboard
      nextcloud-occ app:disable photos

      APPDATA=$(find /var/lib/nextcloud/data/appdata_* -maxdepth 0)
      mkdir -p $APPDATA/external/icons
      cp ${./nextcloud/clip.svg} $APPDATA/external/icons/clip.svg
      cp ${./nextcloud/clip-dark.svg} $APPDATA/external/icons/clip-dark.svg

      source ${config.age.secrets.stats.path}
      echo ${lib.strings.escapeShellArg (builtins.toJSON ext)} > external.json
      sed -i "s/stats.eicas.nl/stats.eicas.nl\/$STATS_OUT/" external.json
      nextcloud-occ config:app:set external sites --value $(cat external.json)
    '';
    after = [ "nextcloud-setup.service" ];
    wantedBy = [ "multi-user.target" ];
  };
}
