{ config, lib, pkgs, ... }:

let
  blackbox-config = pkgs.writeTextFile {
    name = "blackbox.yml";
    text = ''
      modules:
        http_2xx:
          prober: http
          timeout: 5s
          http:
            fail_if_not_ssl: true
            follow_redirects: true
            tls_config:
              insecure_skip_verify: false
            # TODO fix ipv6
            preferred_ip_protocol: ip4
        tcp_connect:
          prober: tcp
          timeout: 5s
          tcp:
            # TODO fix ipv6
            preferred_ip_protocol: ip4
    '';
  };
in
{
  services.prometheus = {
    enable = true;
    retentionTime = "5y";
    exporters.blackbox = {
      enable = true;
      configFile = blackbox-config;
    };
    exporters.nextcloud = {
      enable = true;
      url = "https://cloud.eicas.nl";
      tokenFile = ./secrets/nctoken.txt;
    };
    # requires https://github.com/NixOS/nixpkgs/pull/243338
    # from branch prometheus-imap-mailstat-exporter-init-at-2023-06-20
    exporters.imap-mailstat = {
      enable = true;
      oldestUnseenDate = true;
      accounts = import secrets/imap-mailstat-exporter.conf.nix;
    };
    globalConfig = {
      scrape_interval = "1m";
    };
    scrapeConfigs = [
      {
        job_name = "blackbox";
        metrics_path = "/probe";
        params.module = [ "http_2xx" ];
        static_configs = [{
          targets = [
            "https://www.eicas.nl/dashboard_6acxsbaa.txt"
            "https://cloud.eicas.nl/robots.txt"
          ];
        }];
        relabel_configs = [{
          source_labels = [ "__address__" ];
          target_label = "__param_target";
        } {
          source_labels = [ "__param_target" ];
          target_label = "instance";
        } {
          target_label = "__address__";
          replacement = "localhost:9115";
        }];
      }
      {
        job_name = "cloud";
        static_configs = [{
          targets = [
            "127.0.0.1:${toString config.services.prometheus.exporters.nextcloud.port}"
          ];
        }];
      }
      {
        job_name = "imap";
        scrape_interval = "1h";
        static_configs = [{
          targets = [
            "127.0.0.1:${toString config.services.prometheus.exporters.imap-mailstat.port}"
          ];
        }];
      }
    ];
  };
  services.grafana = {
    enable = true;
    settings.server = {
      http_addr = "127.0.0.1";
      http_port = 4000;
      domain = "monitoring.eicas.nl";
    };
  };
}
