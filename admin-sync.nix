{ config, lib, pkgs, admin-sync-src, ... }:

let
  admin-sync = pkgs.callPackage admin-sync-src {};
in
{
  systemd.services.update-addressbook = {
    confinement = {
      enable = true;
    };
    script = ''
      cp ${./secrets/dotenv} ./.env
      rm -r cache || true
      ${admin-sync}/bin/generate-address-book ${config.age.secrets.mailinglistsYaml.path} .
      ${pkgs.postgresql}/bin/psql roundcube < refresh.sql
    '';
    serviceConfig.RuntimeDirectory = "update-addressbook";
    serviceConfig.WorkingDirectory = "/run/update-addressbook";
    serviceConfig.BindReadOnlyPaths = [
      "/etc/resolv.conf"
      "/etc/passwd"
      "/run/postgresql"
      config.age.secrets.mailinglistsYaml.path
    ];

    # To connect to postgres
    serviceConfig.User = "roundcube";

    serviceConfig.Type = "oneshot";
    serviceConfig.TimeoutStartSec = 2400;
  };
  systemd.services.update-signatures = {
    confinement = {
      enable = true;
    };
    script = ''
      cp ${./secrets/dotenv} ./.env
      rm -r cache || true
      cp ${admin-sync}/usr/share/footer.png .
      ${admin-sync}/bin/generate-signatures ${config.age.secrets.mailinglistsYaml.path} ${config.age.secrets.personalYaml.path}
      ${pkgs.postgresql}/bin/psql roundcube < signatures.sql
    '';
    serviceConfig.RuntimeDirectory = "update-signatures";
    serviceConfig.WorkingDirectory = "/run/update-signatures";
    serviceConfig.BindReadOnlyPaths = [
      "/etc/resolv.conf"
      "/etc/passwd"
      "/run/postgresql"
      config.age.secrets.mailinglistsYaml.path
      config.age.secrets.personalYaml.path
    ];

    # To connect to postgres
    serviceConfig.User = "roundcube";

    serviceConfig.Type = "oneshot";
    serviceConfig.TimeoutStartSec = 2400;
  };
  systemd.services.generate-smoelenboek = {
    confinement = {
      enable = true;
    };
    path = [ pkgs.imagemagick ];
    script = ''
      # Load-bearing comment: makes sure imagemagick and its dependencies
      # gets added to BindReadOnlyPaths:
      # ${pkgs.imagemagick}

      rm -r images cache || true
      source ${config.age.secrets.nc-roundcube.path}
      echo "NC_TOKEN=$NC_TOKEN" > .env
      ${admin-sync}/bin/generate-smoelenboek ${config.age.secrets.mailinglistsYaml.path} 

      echo Uploading
      ${pkgs.curl}/bin/curl -H "Authorization: Bearer $NC_TOKEN" --upload-file smoelenboek.pdf https://cloud.eicas.nl/remote.php/dav/files/$NC_USER/Algemeen/smoelenboek.pdf
      echo Done uploading
    '';
    serviceConfig.RuntimeDirectory = "update-smoelenboek";
    serviceConfig.WorkingDirectory = "/run/update-smoelenboek";
    serviceConfig.BindReadOnlyPaths = [
      # to resolve cloud.eicas.nl
      "/etc/resolv.conf"
      "/etc/ssl"
      "/etc/static/ssl"
      pkgs.cacert
      config.age.secrets.nc-roundcube.path
      config.age.secrets.mailinglistsYaml.path
    ];

    # to access mailinglistsYaml
    serviceConfig.User = "roundcube";

    serviceConfig.Type = "oneshot";
    serviceConfig.TimeoutStartSec = 2400;
  };

  systemd.timers.generate-smoelenboek = {
    partOf = [ "generate-smoelenboek.service" ];
    wantedBy = [ "timers.target" ];
    timerConfig.OnUnitActiveSec = 3600;
    timerConfig.OnBootSec = 900;
    timerConfig.RandomizedDelaySec = 900;
  };

  systemd.services.generate-naw-export = {
    script = ''
      cp ${./secrets/dotenv} ./.env
      ${admin-sync}/bin/generate-naw-export

      source ${config.age.secrets.nc.path}
      ${pkgs.curl}/bin/curl -H "Authorization: Bearer $NC_TOKEN" --upload-file naw.xls https://cloud.eicas.nl/remote.php/dav/files/$NC_USER/Personeelsmanagement/Overzicht%20medewerkers/naw.xlsx
    '';
    serviceConfig.Type = "oneshot";
    serviceConfig.TimeoutStartSec = 2400;
  };

  systemd.timers.generate-naw-export = {
    partOf = [ "generate-naw-export.service" ];
    wantedBy = [ "timers.target" ];
    timerConfig.OnBootSec = 1200;
    timerConfig.OnUnitActiveSec = 7200;
  };

  systemd.services.generate-verjaardagskalender = {
    script = ''
      cp ${./secrets/dotenv} ./.env
      ${admin-sync}/bin/generate-verjaardagskalender
      ${admin-sync}/bin/generate-teams

      source .env
      ${pkgs.curl}/bin/curl --upload-file verjaardagskalender.pdf https://$NC_USER:$NC_PASS@cloud.eicas.nl/remote.php/dav/files/balie/Algemeen/verjaardagskalender.pdf
      ${pkgs.curl}/bin/curl --upload-file teams.pdf https://$NC_USER:$NC_PASS@cloud.eicas.nl/remote.php/dav/files/balie/Algemeen/teams.pdf
    '';
    serviceConfig.Type = "oneshot";
    serviceConfig.TimeoutStartSec = 2400;
  };

  systemd.services.generate-maandplanning = {
    script = ''
      cp ${./secrets/dotenv} ./.env
      rm -r cache || true
      ${admin-sync}/bin/generate-maandplanning

      source .env
      ${pkgs.curl}/bin/curl --upload-file maandplanning.pdf https://$NC_USER:$NC_PASS@cloud.eicas.nl/remote.php/dav/files/balie/Algemeen/Suppoosten/maandplanning.pdf
    '';
    serviceConfig.Type = "oneshot";
    serviceConfig.TimeoutStartSec = 2400;
  };

  systemd.timers.update-addressbook = {
    partOf = [ "update-addressbook.service" ];
    wantedBy = [ "timers.target" ];
    timerConfig.OnBootSec = 1200;
    timerConfig.OnUnitActiveSec = 7200;
  };

  systemd.timers.generate-verjaardagskalender = {
    partOf = [ "generate-verjaardagskalender.service" ];
    wantedBy = [ "timers.target" ];
    timerConfig.OnBootSec = 600;
    timerConfig.OnUnitActiveSec = 3600;
  };

  systemd.timers.generate-stats = {
    partOf = [ "generate-stats.service" ];
    wantedBy = [ "timers.target" ];
    # new museumkaart stats typically come in at 10:00
    timerConfig.OnCalendar = "10:15";
  };
  systemd.timers.generate-maandplanning = {
    partOf = [ "generate-maandplanning.service" ];
    wantedBy = [ "timers.target" ];
    timerConfig.OnBootSec = 600;
    timerConfig.OnUnitActiveSec = 3600;
  };


  systemd.services.generate-stats = {
    script = ''
      rm .env
      cp ${config.age.secrets.stats.path} ./.env
      rm -r cache/zettle-* || true
      ${admin-sync}/bin/bezoekersrapportage

      rm -r out || true
      source .env
      mkdir -p out/$STATS_OUT
      cp -r ${admin-sync}/usr/share/web/* out/$STATS_OUT
      cp -r data out/$STATS_OUT
    '';
    serviceConfig.User = "stats";
    serviceConfig.RuntimeDirectory = "stats";
    serviceConfig.WorkingDirectory = "/var/lib/stats";
    serviceConfig.Type = "oneshot";
    serviceConfig.TimeoutStartSec = 2400;
  };
  users.users.stats = {
    isSystemUser = true;
    group = "stats";
  };
  users.groups.stats = {};
  services.nginx.virtualHosts."stats.eicas.nl" = {
    forceSSL = true;
    enableACME = true;
    # force DNS-01 validation
    acmeRoot = null;
    http2 = true;
    root = "/var/lib/stats/out";
    extraConfig = ''
      expires 30m;
      add_header Cache-Control "public";
    '';
  };
}
