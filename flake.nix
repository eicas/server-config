{
  description = "EICAS server configurations";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    #nixpkgs.url = "git+file:///home/aengelen/nixpkgs";
    pisignage-server = {
      url = "git+https://codeberg.org/raboof/nix-pisignage";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    admin-sync-src = {
      url = "git+https://codeberg.org/eicas/admin-sync";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, pisignage-server, agenix, admin-sync-src }@attrs: {

    nixosConfigurations.hetzner = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = attrs;
      modules = [
        ./configuration.nix
        agenix.nixosModules.default
      ];
    };
    devShells."x86_64-linux".default =
      nixpkgs.legacyPackages."x86_64-linux".mkShell {
        packages = [
          # TODO migrate everything to agenix
          nixpkgs.legacyPackages."x86_64-linux".git-crypt
          agenix.packages."x86_64-linux".default
        ];
      };
  };
}
