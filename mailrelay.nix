{ config, lib, pkgs, ... }:

with lib;

let
  # the values of the options set for the service by the user of the service
  cfg = config.services.mailrelay;
  nginx = (pkgs.nginxMainline.override {
    withMail = true;
    withDebug = true;
  }).overrideAttrs(a: {
    patches = a.patches ++ [
      # Allow returning 'Auth-Method: none' from the auth server
      # to the nginx mail proxy, to avoid trying to authenticate to
      # the target server
      ./nginx-no-proxy-auth.patch
    ];
  });
  startscript = pkgs.writeScript "start.sh" ''
    # Kill all children on exit, also the auth server
    trap 'jobs -p | xargs kill' EXIT

    ${pkgs.python311}/bin/python3 ${./authserver.py} &
    ${pkgs.stunnel}/bin/stunnel ${./stunnel.conf} &
    ${nginx}/bin/nginx -c ${./nginx.conf}
  '';
in {
  options = {
    services.mailrelay = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable the EICAS mail relay service";
      };
    };
  };
  config = mkIf cfg.enable {
    # TODO use something like sops to restrict access
    # TODO pass 'directly' to service instead of using /etc
    environment.etc = {
      "mailrelay/cert.pem".source = ./secrets/cert.pem;
      "mailrelay/fullchain.pem".source = ./secrets/fullchain.pem;
      "mailrelay/privkey.pem".source = ./secrets/privkey.pem;
      "mailrelay/creds.txt".source = ./secrets/creds.txt;
    };
    systemd.services.mailrelay = {
      after = [ "network-online.target" ];
      requires = [ "network-online.target" ];
      before = [ "multi-user.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        ExecStart = "${pkgs.bash}/bin/bash ${startscript}";
        StandardOutput = "journal";
      };
      restartTriggers = [
        (builtins.hashFile "sha256" ./secrets/cert.pem)
        (builtins.hashFile "sha256" ./secrets/fullchain.pem)
        (builtins.hashFile "sha256" ./secrets/privkey.pem)
        (builtins.hashFile "sha256" ./secrets/creds.txt)
      ];
    };
  };
}
