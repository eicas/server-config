{ pkgs ? import <nixpkgs> {} }:

let
  sbtixSrc = pkgs.fetchFromGitHub {
    owner = "natural-transformation";
    repo = "sbtix";
    rev = "f997a32642f5884d2a582e13c41250bfa84a1123";
    hash = "sha256-5AnX3W/uf8hL6JTBt6lKXt/3soQRBU4s77xv36nPwCc=";
  };
  sbtix = pkgs.callPackage "${sbtixSrc}/plugin/nix-exprs/sbtix.nix" {};
in
  sbtix.buildSbtProgram {
    name = "calendar-export";
    src = pkgs.fetchFromGitea {
      domain = "codeberg.org";
      owner = "eicas";
      repo = "calendar-export";
      rev = "95f8c12958bc4e8bb10cccf3ccf7e1a7a30f4d9e";
      hash = "sha256-vc/9aGjJkl3rACN562C9e+PKvMZ5lq+z2W7tdWRQtsQ=";
    };
    repo = [
      (import ./repo.nix)
      (import ./project/repo.nix)
      (import ./manual-repo.nix)
    ];
    #src = fetchFromGitea {
    #  domain = "codeberg.org";
    #  owner = "eicas";
    #  repo = "calendar-export";
    #  rev = "a95a0e13d4c5b68eaaab95e62b475e0567ba8dbe"
    #};
  }
