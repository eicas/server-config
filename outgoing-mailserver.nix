{ config, lib, pkgs, ... }:

{
  # Used by postfix to sign outgoing mail
  services.opendkim = {
    enable = true;
    selector = "hz";
    domains = "csl:eicas.nl";
    # Allow all of the opendkim group to connect to the socket
    configFile = pkgs.writeText "opendkim.conf" (''
      UMask 0007
    '');
  };
  services.postfix = {
    enable = true;
    hostname = "mail.eicas.nl";
    enableSmtp = true;
    config = {
      # No local delivery
      mydestination = "";

      # DKIM signing
      smtpd_milters = [ "unix:/run/opendkim/opendkim.sock"];
      non_smtpd_milters = [ "unix:/run/opendkim/opendkim.sock" ];
    };
    extraConfig = ''
      inet_interfaces = loopback-only
    '';
  };
  users.users.postfix.extraGroups = [ "opendkim" ];
}
